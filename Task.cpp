#include "Task.h"
#include "ui_Task.h"

#include <QInputDialog>
#include <QDebug>
#include <QFile>

Task::Task(const QString& name, QWidget *parent) :
        QWidget(parent),
        ui(new Ui::Task)
{
    ui->setupUi(this);
    this->ui->label_due->setText("due: ");
    ui->dateEdit->setMinimumDate(QDate::currentDate());
    setName(name);
    setDeadline(QDate::currentDate());

    connect(ui->editButton, &QPushButton::clicked, this, &Task::rename);
    connect(ui->removeButton, &QPushButton::clicked, [this] {
        emit removed(this);
    });
    connect(ui->checkbox, &QCheckBox::toggled, this, &Task::checked);
    connect(ui->dateEdit, &QDateEdit::dateChanged, this, &Task::deadlineChanged);
    connect(ui->dateEdit, &QDateEdit::editingFinished, this, &Task::deadlineChanged);
    deadlineChanged();
}

Task::~Task()
{
    qDebug() << "~Task() called";
    delete ui;
}

void Task::setDeadline(const QDate& deadline)
{
    this->ui->dateEdit->setDate(deadline);
}

QDate Task::deadline() const
{
    return ui->dateEdit->date();
}

void Task::setName(const QString& name)
{
    ui->checkbox->setText(name);
}

QString Task::name() const
{
    return ui->checkbox->text();
}

bool Task::isCompleted() const
{
   return ui->checkbox->isChecked();
}

void Task::rename()
{
    bool ok;
    QString value = QInputDialog::getText(this, tr("Edit task"),
                                          tr("Task name"), QLineEdit::Normal,
                                          this->name(), &ok);
    if (ok && !value.isEmpty()) {
        setName(value);
    }
}

void Task::checked(bool checked)
{
    QFont font(ui->checkbox->font());
    font.setStrikeOut(checked);
    ui->checkbox->setFont(font);

    emit statusChanged(this);
}

void Task::deadlineChanged()
{
    //calculate date difference
    qint64 daydiff=deadline().toJulianDay()-QDate::currentDate().toJulianDay();
    QFile file;
    //read and apply styling
    if ( daydiff <= 1)
    {
        file.setFileName(":/deadline_critical.qss");
    }
    else if ( daydiff <= 4)
    {
        file.setFileName(":/deadline_normal.qss");
    }
    else
    {
        file.setFileName(":/deadline_default.qss");
    }
    file.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(file.readAll());
    ui->dateEdit->setStyleSheet(styleSheet);
}

