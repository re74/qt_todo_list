# Qt_todo_list



## Description

This project is an assignment in C++ course. The assignment is based on the code presented [here](https://gitlab.com/plczapiewski/qt_todo_list). 

## Overview

The feature included into the existing codebase is deadline.  
- The deadline is marked with green, yellow and red depending on how close the deadline is.
![Example](pics/general.jpg)
- The dates before the current date are imposiible to assign. 
![Example](pics/date_picker.jpg)

## Maintainers
@MariaNema
